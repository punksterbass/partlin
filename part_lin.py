import random

def port(x,y):
  if abs(x-y) <= 2:
    return True
  else:
    return False

def remove_port(x,linv):
  prov = []
  for y in linv:
    if port(y,x):
      prov.append(y)

  for el in prov:
    linv.remove(el)
    
    
def part_lin(training, lin=False):
    result = ["N"]
    linvector = [training[0]]
    result2 = []
    result2.append([training[0]])
  
    for i in range(1,len(training)):
        current = training[i]
        previous = training[i-1]
        if current in linvector:
            if current == linvector[-1]:
                result.append("R")
            else:
                result.append("G")
            result2.append([x for x in linvector])
        else:
            port_bool = [port(current,x) for x in linvector]
            if port_bool.count(True) == 0:
                result.append("N")
                linvector.append(current)
                result2.append([x for x in linvector])
            else:
                if port_bool.count(True) == 1:
                    if port(current,previous):
                        result.append("P")
                        linvector.remove(previous)
                        linvector.append(current)
                        result2.append([x for x in linvector])
                    else:
                        result.append("A")
                        remove_port(current,linvector)
                        linvector.append(current)
                        result2.append([x for x in linvector])
                else:
                    if port(current,previous):
                        result.append("C")
                    else:
                        result.append("F")
                        remove_port(current,linvector)
                        linvector.append(current)
                        result2.append([x for x in linvector])

    if lin:
        return result2
    else:
        return result


def simple_analysis(train):
  result = ["I"]
  for indice in range(1,len(train)):
    current, previous = train[indice], train[indice - 1]
    diff = abs(current - previous)
    if diff == 0:
      result.append("R")
    elif diff <= 2:
      result.append("P")
    else:
      result.append("N")
  return result


def create_simple_melody(analysis, start_pitch, available_pitches):
  result = [start_pitch]
  for i in range(1,len(analysis)):
    while True:
      new_note = random.choice(available_pitches)
      result.append(new_note)
      if simple_analysis(result) == analysis[0:i+1]:
        break
      else:
        result.pop()
  return result


def port_list(pitch, pitch_list):
    result = []
    for x in pitch_list:
        if port(pitch,x):
            result.append(x)
    return result


def next_state(result_list, desired_state, available_pitches):
    current_linvector = part_lin(result_list, lin=True)[-1]
    last_pitch = result_list[-1]
    if desired_state == "R":
        return current_linvector[-1]
    elif desired_state == "P":
        candidates = [x for x in available_pitches if port(x,last_pitch) and
                                                   (x != last_pitch and
                                                   len(port_list(x,current_linvector)) == 1)]
        if len(candidates) == 0:
            return False
        else:
            return random.choice(candidates)
    elif desired_state == "N":
        candidates = [x for x in available_pitches if len(port_list(x,current_linvector)) == 0]
        if len(candidates) == 0:
            return False
        else:
            return random.choice(candidates)
    elif desired_state == "G":
        candidates = [x for x in available_pitches if x in current_linvector[0:-1]]
        if len(candidates) == 0:
            return False
        else:
            return random.choice(candidates)
    elif desired_state == "A":
        candidates = [x for x in available_pitches if 
                  len(port_list(x,current_linvector)) == 1 and
                   (x not in current_linvector and port(x,last_pitch) == False)]
        if len(candidates) == 0:
            return False
        else:
            return random.choice(candidates)
    else:
        candidates = [x for x in available_pitches if len(port_list(x,current_linvector)) == 2]
        if len(candidates) == 0:
            return False
        else:
            if desired_state == "F":
                new_candidates = [x for x in candidates if port(x,last_pitch) == False]
            elif desired_state == "C":
                new_candidates = [x for x in candidates if port(x,last_pitch)]
            if len(new_candidates) == 0:
                return False
            else:
                return random.choice(new_candidates)


def part_melody(model, available_pitches, start_pitch, fail_counter_num=100):
    result = [start_pitch]
    i = 1
    fail_counter = 0
    while len(result) < len(model):
        next_pitch = next_state(result, model[i], available_pitches)
        if next_pitch:
            result.append(next_pitch)
            i += 1
        else:
            fail_counter += 1
            if fail_counter == fail_counter_num:
                break

    if fail_counter < 100:
        return result
    else:
        return False
    
